<?php

namespace Drupal\messenger_bot_api\Plugin\Chatbot;

use Drupal\chatbots_api\ChatBotsBotBase;
use Drupal\chatbots_api\Entity\ChatBot;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Url;

/**
 * Class MessengerBot.
 *
 * @Chatbot(
 *   id = "messenger_bot",
 *   label = @Translation("Messenger Bot"),
 *   description = @Translation("Provides Messenger API integration")
 * )
 */
class MessengerBot extends ChatBotsBotBase {

  /**
   * {@inheritdoc}
   */
  public function extendForm(array $form, FormStateInterface $form_state, ChatBot $entity) {
    $options = [
      '@page_token' => Link::fromTextAndUrl($this->t('page access token'), Url::fromUri('https://developers.facebook.com/docs/messenger-platform/guides/setup#page_access_token'))->toString(),
      '@verify_token' => Link::fromTextAndUrl($this->t('verify token'), Url::fromUri('https://developers.facebook.com/docs/messenger-platform/getting-started/webhook-setup#verification'))->toString(),
    ];

    // TODO: Validation possible?
    $form['page_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Page token'),
      '#default_value' => $entity->get('page_token'),
      '#description' => $this->t('Page token is required to start using the APIs. See @page_token for details.', $options),
    ];

    // TODO: Validation possible?
    $form['verify_token'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Verify token'),
      '#default_value' => $entity->get('verify_token'),
      '#description' => $this->t('This token will be used to register webhooks. See @verify_token for details.', $options),
    ];

    return $form;
  }

}
